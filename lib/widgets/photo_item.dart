import 'package:flutter/material.dart';
import 'package:phonestore/models/phone-model.dart';

class PhoneItem extends StatelessWidget {
  final PhoneModel phoneModel;
  final ValueChanged<PhoneModel> onClick;

  PhoneItem({
    required this.phoneModel,
    required this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      key: UniqueKey(),
      behavior: HitTestBehavior.opaque,
      onTap: () => onClick(phoneModel),
      child: Column(
        //margin: const EdgeInsets.all(4),
        children:
        [ Image.network(
          phoneModel.image,
          fit: BoxFit.cover,
        
        ),
        
        Text(phoneModel.manufacturer), 
        Text(phoneModel.price.toString()) ,
        Text(phoneModel.model) 
        ],
      ),
    );
  }
}
