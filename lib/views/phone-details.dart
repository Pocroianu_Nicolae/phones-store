import 'package:flutter/material.dart';
import 'package:phonestore/models/phone-model.dart';

class PhoneDetails extends StatelessWidget {
  final PhoneModel phoneModel;

  const PhoneDetails({
    Key? key,
    required this.phoneModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // leading: TextButton(
        //   child: Image.asset(
        //     'icons/left_arrow.png',
        //     fit: BoxFit.cover,
        //     width: 30,
        //   ),
        //   onPressed: () {
        //     Navigator.pop(context);
        //   },
        // ),
        title: Center(
          child: Text(
            '',
            style: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        elevation: 1,
        bottomOpacity: 0,
      ),
      body: Center(
        child: Container(
          child: Image.network(
            phoneModel.image,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
