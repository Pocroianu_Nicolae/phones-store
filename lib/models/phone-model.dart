class PhoneModel {
  late int id;
  late String manufacturer;
  late String model;
  late String operatingSystem;
  late String dimensions;
  late String color;
  late String memory;
  late Object specs;
  late String image;
  late int price;

  PhoneModel({
    required this.id,
    required this.manufacturer,
    required this.model,
    required this.operatingSystem,
    required this.dimensions,
    required this.color,
    required this.memory,
    required this.specs,
    required this.image,
    required this.price,
  });

  factory PhoneModel.fromJson(Map<String, dynamic> parsedJson) {
    return PhoneModel(
        id: int.parse(parsedJson['id']),
        manufacturer: parsedJson['manufacturer'],
        model: parsedJson['model'],
        operatingSystem: parsedJson['operatingSystem'],
        dimensions: parsedJson['dimensions'],
        color: parsedJson['color'],
        memory: parsedJson['memory'],
        specs: parsedJson['specs'],
        image: parsedJson['image'],
        price: int.parse(parsedJson['price']));
  }
}
